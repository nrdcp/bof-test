# SOLUTION
---

## Estimation
---

Estimated: 4 hours

Spent: 8 hours (due to additional changes to project structure)

## Solution
---

### Project scope
Only **TASK 1** from *untouched* ```README.md``` was implemented.

### Project structure
I have changed the setup to something that is a good candidate for a production-ready project. 

Boilerplate is based on [https://github.com/AngularClass/NG6-starter]
(https://github.com/AngularClass/NG6-starter), with some enhancements based on my personal preference.

Tech setup: Angular 1.5, SASS, ES6 via Babel, Webpack, ESLint.

Styleguide followed: [AngularJS styleguide (ES2015)](https://github.com/toddmotto/angular-styleguide) by Todd Motto

### Running code

``` npm install ```

``` npm run serve ```

Open ```http://localhost:3000/``` in the browser; Development json-server is available on ```http://localhost:3004/```

### Running tests
I have included a couple of unit tests using Karma+Mocha+Chai. Those can be run by executing ```npm run test```

### Building the project
Out of scope, however some boilerplate exists, but likely needs some tuning.

### Other comments

Search Form  TODOS:

* At present search form requires to be submitted explicitly; depending on the number of fields and usage patterns 'search on change' should be implemented

* Any relevant form fields' validation should be implemented as well as user-friendly validation errors

* To save on unnecessary calls tosearch endpoint results for previous searches within the interaction session could be cached locally

Search Results TODOS:

* UI should provide feedback while search activity is in progress (e.g. an animated spinner, greyed out and disable previous search results)

* Pagination is static and non-functional. A more elaborate version (e.g. < 1 ... 14, **15**, 16 ... 31 >) should be implemented if the data source has more than 8-10 pages worth of rows. 

* Allowing the user to select how many results to display per page could be useful in some cases (depends on user stories)
