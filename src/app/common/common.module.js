import angular from 'angular';
import HeaderComponent from './components/header/header.component';

const commonModule = angular
  .module('app.common', [])
  .component('header', HeaderComponent)
  .name;

export default commonModule;
