import angular from 'angular';
import HomeModule from './home/home.module';

const componentsModule = angular
  .module('app.components', [
    HomeModule,
  ])
  .name;

export default componentsModule;
