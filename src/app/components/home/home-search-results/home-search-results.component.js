import template from './home-search-results.html';
import controller from './home-search-results.controller';

import './home-search-results.scss';

const HomeSearchResultsComponent = {
  bindings: {
    items: '<',
  },
  template,
  controller,
};

export default HomeSearchResultsComponent;
