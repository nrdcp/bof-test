
class HomeController {
  constructor(homeSearchService) {
    'ngInject';

    this.searchResults = [];
    this.homeSearchService = homeSearchService;
  }

  onSearchReady(formData) {
    this.homeSearchService.search(formData)
      .then((data) => {
        this.searchResults = data;
      })
      .catch((e) => {
        console.log(e);
        // TODO: handle errors
      });
  }
}

export default HomeController;
