import constants from './home-search.constants';

class HomeSearchTransformer {

  getThumbnailUrl(assets = []) {
    let baseUrl = constants.PROFILE_THUMBNAIL_URL;

    if (!assets.length) {
      return '';
    }

    let thumbnail = assets.find(asset => asset.is_thumbnail === true);
    return thumbnail ? `${baseUrl}${thumbnail.path}` : `${baseUrl}${assets[0].path}`;
  }

  create(data) {
    return {
      id: data.id,
      title: data.profile_title || '',
      subTitle: data.profile_sub_title || '',
      country: data.country_name || '',
      enabled: data.is_enabled || false,
      visible: data.is_visible || false,
      thumbnailUrl: data.assets && data.assets.length ? this.getThumbnailUrl(data.assets) : '',
    };
  }
}

export default HomeSearchTransformer;
