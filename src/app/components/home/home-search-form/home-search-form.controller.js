
class HomeSearchFormController {
  constructor() {
    this.formData = {
      enabled: true,
      visible: true,
      country: '',
      query: '',
    };
  }

  $onInit() {
    this.onSubmit();
  }

  setEnabled(value) {
    this.formData.enabled = value;
  }

  setVisible(value) {
    this.formData.visible = value;
  }

  onSubmit() {
    this.onSearchReady({
      formData: this.formData,
    });
  }
}

export default HomeSearchFormController;
