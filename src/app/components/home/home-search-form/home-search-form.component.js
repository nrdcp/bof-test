import template from './home-search-form.html';
import controller from './home-search-form.controller';

const HomeSearchFormComponent = {
  bindings: {
    onSearchReady: '&',
  },
  template,
  controller,
};

export default HomeSearchFormComponent;
