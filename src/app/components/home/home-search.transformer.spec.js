/* eslint-disable */

import HomeSearchTransformer from './home-search.transformer';

let transformer;

describe('HomeSearchTransformer', function() {
  beforeEach(function() {
    transformer = new HomeSearchTransformer();
  })

  describe('getThumbnailUrl', function() {
    it('returns an empty string if no assets exist', function() {
      expect(transformer.getThumbnailUrl([])).to.equal('');
    });

    it('returns URL to the first thumbnail asset', function() {
      let input = [
        {
          is_thumbnail: false,
          path: '1.jpg'
        },
        {
          is_thumbnail: true,
          path: '2.jpg'
        },
        {
          is_thumbnail: true,
          path: '3.jpg'
        },
      ];

      expect(transformer.getThumbnailUrl(input, '')).to.include('2.jpg');
    });

    it('returns URL to the first asset if no thumbnail assets found', function() {
      let input = [
        {
          is_thumbnail: false,
          path: '1.jpg'
        },
        {
          is_thumbnail: false,
          path: '2.jpg'
        },
        {
          is_thumbnail: false,
          path: '3.jpg'
        },
      ];

      expect(transformer.getThumbnailUrl(input, '')).to.include('1.jpg');
    });
  });

  describe('create', function() {
    it('transforms input data according to rules', function() {
      const input = {
        id: 111,
        profile_title: 'Thomas Anderson',
        profile_sub_title: 'subtitle',
        country_name: 'Japan',
        is_enabled: true,
        is_visible: false,
        assets: []
      };

      const output = {
        id: 111,
        title: 'Thomas Anderson',
        subTitle: 'subtitle',
        country: 'Japan',
        enabled: true,
        visible: false,
        thumbnailUrl: ''
      };

      let result = transformer.create(input);
      expect(result).to.deep.equal(output);
    });

    it('provides fallbacks for non-essential missing input values', function() {
      const input = {
        id: 111,
        is_enabled: true,
        is_visible: false,
      };

      const output = {
        id: 111,
        title: '',
        subTitle: '',
        country: '',
        enabled: true,
        visible: false,
        thumbnailUrl: ''
      };

      let result = transformer.create(input);
      expect(result).to.deep.equal(output);
    });
  });
});