class HomeSearchService {
  constructor($http, homeSearchTransformer) {
    'ngInject';

    this.$http = $http;
    this.homeSearchTransformer = homeSearchTransformer;
  }

  constructUrl(data) {
    let queryParams = [
      (data.query ? `q=${data.query}` : ''),
      (data.country ? `country_name=${data.country}` : ''),
      `is_enabled=${data.enabled}`,
      `is_visible=${data.visible}`,
      '_limit=10',
    ].filter(value => !!value)
      .join('&');
    
    // TODO: move API location URL to common constants
    return `http://localhost:3004/profiles?${queryParams}`;
  }

  search(formData) {
    return this.$http.get(this.constructUrl(formData))
      .then((response) => {
        return response.data
          .map(profile => this.homeSearchTransformer.create(profile));
      })
      .catch((e) => {
        return Promise.reject(e);
      });
  }
}

export default HomeSearchService;
