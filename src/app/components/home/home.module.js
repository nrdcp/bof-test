import angular from 'angular';
import uiRouter from 'angular-ui-router';
import HomeComponent from './home.component';
import HomeSearchFormComponent from './home-search-form/home-search-form.component';
import HomeSearchResultsComponent from './home-search-results/home-search-results.component';
import HomeSearchService from './home-search.service';
import HomeSearchTransformer from './home-search.transformer';

const HomeModule = angular
  .module('home', [
    uiRouter,
  ])
  .config(($stateProvider, $urlRouterProvider) => {
    'ngInject';

    $urlRouterProvider.otherwise('/');

    $stateProvider
      .state('home', {
        url: '/',
        component: 'home',
      });
  })
  .component('home', HomeComponent)
  .component('homeSearchForm', HomeSearchFormComponent)
  .component('homeSearchResults', HomeSearchResultsComponent)
  .service('homeSearchService', HomeSearchService)
  .service('homeSearchTransformer', HomeSearchTransformer)
  .name;

export default HomeModule;
