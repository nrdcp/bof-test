import template from './home.html';
import controller from './home.controller';
import './home.scss';

const HomeComponent = {
  bindings: {},
  template,
  controller,
};

export default HomeComponent;
